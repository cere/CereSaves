# CereSaves
~~(aka Syncthing-CloudSaves)~~

This is a basic little wrapper utility that automatically checks to see if newer savedata exists in some sort of synced directory, and automatically loads in any newer data it finds.

Obviously this is a WIP and not very good, but I wanted a quick and dirty cloud saves solution for non-steam games.

## Installation
```bash
git clone https://gitdab.com/cere/CereSaves.git
cd CereSaves
```
Edit `stcs-global-config.sh`
* `STCS_SAVEGAME_SYNCDIR` should point to the folder being synced by your syncing solution of choice (ie Syncthing, Resilio Sync, etc)
* `STCS_TIMESTAMPS_DIR` should point to a local directory to store a timestamp of each game that's backed up.

```bash
mkdir ~/.local/share/CereSaves/
cp stcs-global-config.sh ~/.local/share/CereSaves/
cp stcs-wrapper.sh ~/.local/bin # Or anywhere else on the $PATH
mkdir "$HOME/.local/share/CereSaves/Timestamps" # Or whatever you changed the value of STCS_TIMESTAMPS_DIR to be
```

## Usage
* Place a copy of `stcs-config.sh.example` next to the game being executed.
* Edit the file, removing the `.example` and filling out the gamename and the location of the savedata to back up. (It can be a file or folder of savedata)
    * Note: you may also define a custom configfile path by setting the environment variable `STCS_GAME_CONFIG` prior to executing the wrapper.
* Prefix any launch command you wish to invoke with `stcs-wrapper.sh`, similar to how you'd load `mangohud`!