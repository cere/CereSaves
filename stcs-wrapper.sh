#!/bin/bash

# Check for Configs
STCS_GLOBAL_CONFIG="$HOME/.local/share/CereSaves/stcs-global-config.sh"

if [ -z ${STCS_GAME_CONFIG+x} ]; then
    logfunny "[CereSaves] Game config not defined, trying default!";
    STCS_GAME_CONFIG="./stcs-config.sh"
else
    logfunny "[CereSaves] Game config is set to '$STCS_GAME_CONFIG'";
fi

if [ ! -f "$STCS_GLOBAL_CONFIG" ]; then
    logfunny "[CereSaves] Global config not found!"
    exit 1
fi

# TODO - find a way to make this not a static filename, for multiple games in one folder.
if [ ! -f "$STCS_GAME_CONFIG" ]; then
    logfunny "[CereSaves] Game config not found!"
    exit 1
fi

source "$STCS_GLOBAL_CONFIG"
source "$STCS_GAME_CONFIG"

STCS_CACHED_TIMESTAMP_PATH="${STCS_TIMESTAMPS_DIR}/${STCS_GAMENAME}.timestamp"
STCS_SYNCED_TIMESTAMP_PATH="${STCS_SAVEGAME_SYNCDIR}/${STCS_GAMENAME}.timestamp"
STCS_SYNCED_SAVADATA_PATH="${STCS_SAVEGAME_SYNCDIR}/${STCS_GAMENAME}"

prelaunch () {
    logfunny "[CereSaves] prelaunch start"
    if checkpretimestamps; then
        # Assume Save Data needs to be restored!
        logfunny "[CereSaves] Update to local savedata detected!"
        if [ -f "$STCS_SAVEPATH" ]; then
            #echo "Savedata $STCS_SAVEPATH is a file!"
            rsync -az --ignore-times "$STCS_SYNCED_SAVADATA_PATH/$(basename $STCS_SAVEPATH)" "$STCS_SAVEPATH"
        fi
        if [ -d "$STCS_SAVEPATH" ]; then
            #echo "Savedata $STCS_SAVEPATH is a folder!"
            rsync -az --ignore-times "$STCS_SYNCED_SAVADATA_PATH/$(basename $STCS_SAVEPATH)/" "$STCS_SAVEPATH/"
        fi
        rsync -az --ignore-times "$STCS_SYNCED_TIMESTAMP_PATH" "$STCS_CACHED_TIMESTAMP_PATH"
    fi
    logfunny "[CereSaves] prelaunch complete"
}

postlaunch () {
    logfunny "[CereSaves] postlaunch start"
    # Create Backup Dir if it doesn't exist.
    if [ ! -d "$STCS_SYNCED_SAVADATA_PATH" ]; then
        mkdir "$STCS_SYNCED_SAVADATA_PATH"
    fi
    # Back up Save Data
    if [ -f "$STCS_SAVEPATH" ]; then
        #echo "Savedata $STCS_SAVEPATH is a file!"
        rsync --ignore-times -az "$STCS_SAVEPATH" "$STCS_SYNCED_SAVADATA_PATH/"
    fi
    if [ -d "$STCS_SAVEPATH" ]; then
        #echo "Savedata $STCS_SAVEPATH is a folder!"
        if [ -d "$STCS_SYNCED_SAVADATA_PATH/$(basename $STCS_SAVEPATH)/" ]; then
            mkdir "$STCS_SYNCED_SAVADATA_PATH/$(basename $STCS_SAVEPATH)/"
        fi
        rsync --ignore-times -az "$STCS_SAVEPATH/" "$STCS_SYNCED_SAVADATA_PATH/$(basename $STCS_SAVEPATH)/"
    fi
    local TIMESTAMP=$(date +%s)
    echo "$TIMESTAMP" | tee "$STCS_SYNCED_TIMESTAMP_PATH"
    logfunny "[CereSaves] New sync timestamp is $TIMESTAMP"
    logfunny "[CereSaves] postlaunch complete"
}

checkpretimestamps () {
    if [ ! -f "$STCS_CACHED_TIMESTAMP_PATH" ]; then
        # Cached timestamp does not exist.
        echo "1" | tee "$STCS_CACHED_TIMESTAMP_PATH"
    fi
    if [ ! -f "$STCS_SYNCED_TIMESTAMP_PATH" ]; then
        # Synced timestamp does not exist
        echo "0" | tee "$STCS_SYNCED_TIMESTAMP_PATH"
    fi
    if [ $(cat "$STCS_SYNCED_TIMESTAMP_PATH") -gt $(cat "$STCS_CACHED_TIMESTAMP_PATH") ]; then
        # Update Needed!
        return 0
    else
        return 1
    fi
}

logfunny () {
    if [ -z ${STCS_DEBUG_LOG_FILE+x} ]; then
        STCS_DEBUG_LOG_FILE="./stcs-log.log"
    fi

    if [ ${STCS_DEBUG_LOG} -eq "1" ]; then
        echo "$@" | tee -a "$STCS_DEBUG_LOG_FILE"
    else
        echo "$@"
    fi
}

prelaunch;
"$@";
postlaunch;
logfunny "[CereSaves] script complete"